mod operations;
pub mod yaml;

fn delete_operation(text: String, separator: &str, args: &yaml_rust::yaml::Yaml) -> String {
    let string = &args["string"].as_str().unwrap();
    operations::delete(text, separator, string)
}

fn substitute_operation(text: String, separator: &str, args: &yaml_rust::yaml::Yaml) -> String {
    let old_string = &args["old_string"].as_str().unwrap();
    let new_string = &args["new_string"].as_str().unwrap();
    let excludes = if args["excludes"].is_badvalue() {
        Vec::new()
    } else {
        args["excludes"].as_vec().unwrap()
            .iter()
            .map(|exclude| exclude.as_str().unwrap().to_owned())
            .collect::<Vec<String>>()
    };
    operations::substitute(text, separator, old_string, new_string, excludes)
}

pub fn apply_rules(text: String, separator: &str, map: &yaml::HashMap, debug: bool) -> String {
    let mut modified_text = text.clone();
    for i in 1..=map.map.keys().len() {  // This loop is needed to sort rules application
        let rule = format!("rule{}", i);
        if debug {
            println!("rule: {:?}", rule);
        }
        let key = yaml::HashKey::from(rule.clone());
        match map.map.get(&key.key) {  // check key exists
            Some(value) => {
                let args = &value["args"];
                let operation = &value["operation"];
                if debug {
                    println!("operation: {:?}", operation);
                    println!("args: {:?}", args);
                }
                modified_text = match operation.as_str().unwrap() {
                    "delete" => delete_operation(modified_text, separator, args),
                    "substitute" => substitute_operation(modified_text, separator, args),
                    _ => {
                        println!("Unrecognised operation: {:?}", operation);
                        modified_text
                    },
                }
            },
            _ => {
                println!("rule: {:?} doesn't exist", rule);
                println!("Enumerate rules in continuous: values must be between rule1 and {:?}", format!("rule{}", map.map.keys().len()));
                return text;
            },
        }
    }
    modified_text
}
