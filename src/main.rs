use std::fs;
use std::path::{ Path, PathBuf };
use structopt::StructOpt;

extern crate undust;
use undust::{ apply_rules
            , yaml::{ Config, HashMap }};

#[derive(Debug, StructOpt)]
#[structopt(name="undust", about = "a configurable cli tool for text manipulation")]
struct Opt {
    /// Activate debug mode
    #[structopt(short, long)]
    debug: bool,

    /// Yaml file
    #[structopt(short, long, parse(from_os_str)/* , default_value = "" Default config file?? */)]
    config: PathBuf,

    /// Input file
    #[structopt(short, long, parse(from_os_str))]
    input: PathBuf,
}

fn read_file(path: &Path) -> Result<String, String> {
    if !Path::new(path).exists() {
        Err(format!("path {:?} doesn't exist.", path))
    } else if !Path::new(path).is_file() {
        Err(format!("path {:?} is not a file.", path))
    } else {
        let error = Err(format!("problem when trying to read file {:?}", path));
        fs::read_to_string(path).or(error)
    }
}

fn run_app(yaml_path: &Path, file_path: &Path, debug: bool) -> Result<(), String> {
    let backup_text = match read_file(file_path) {
        Ok(content) => content,
        Err(error) => return Err(error),
    };

    let yaml_content = match read_file(yaml_path) {
        Ok(content) => content,
        Err(error) => return Err(error),
    };

    let mut map = HashMap::from(yaml_content);
    let config = Config::from(&mut map);
    if debug {
        println!("config: {:?}", config);
        println!("map: {:?}", map);
    }

    let text = apply_rules(backup_text.clone(), config.separator.as_str(), &map, debug);

    if text.eq(&backup_text) {
        Ok(())  // don't write the file if its content doesn't change
    } else {
        let error = Err(format!("problem when trying to write file {:?}", file_path));
        fs::write(file_path, text).or(error)
    }
}

fn main() {
    let opt = Opt::from_args();
    if opt.debug {
        println!("cli args: {:?}", opt);
    }

    let debug = opt.debug;
    let file_path = opt.input;
    let yaml_path = opt.config;

    std::process::exit(match run_app(&yaml_path, &file_path, debug) {
        Ok(_) => 0,
        Err(err) => {
            eprintln!("error: {:?}", err);
            1
        }
    });
}
