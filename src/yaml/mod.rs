extern crate yaml_rust;
use yaml_rust::{ YamlLoader };

#[derive(Debug)]
pub struct HashKey {
    pub key: yaml_rust::yaml::Yaml,
}

impl HashKey {
    pub fn from(string: String) -> Self {
        Self { key: yaml_rust::yaml::Yaml::String(string) }
    }
}

#[derive(Debug)]
pub struct HashMap {
    pub map: yaml_rust::yaml::Hash,
}

impl HashMap {
    pub fn from(yaml_content: String) -> Self {
        let docs = YamlLoader::load_from_str(&yaml_content).expect("Something went wrong in yaml parsing");
        let doc = &docs[0];  // Multi document support, doc is a yaml::Yaml
        let map = doc.clone().into_hash().expect("Something went wrong in transition from yaml to hash");
        Self { map }
    }
}

#[derive(Debug)]
pub struct Config {
    pub debug: bool,
    pub separator: String,
}

impl Config {
    pub fn from(map: &mut HashMap) -> Self {
        let config_key = HashKey::from(String::from("config"));
        let mut debug = false;
        let mut separator = String::from("\n");
        if let Some(config) = map.map.remove(&config_key.key) {
            if !config.is_badvalue() {
                if !config["debug"].is_badvalue() {
                    debug = config["debug"].as_bool().unwrap();
                }
                if !config["separator"].is_badvalue() {
                    separator = String::from(config["separator"].as_str().unwrap());
                }
            }
        }
        Self { debug, separator }
    }
}
