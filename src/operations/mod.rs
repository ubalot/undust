use regex::Regex;

pub fn delete(text: String, separator: &str, string: &str) -> String {
    let re = Regex::new(string).unwrap();
    text
        .split(separator)
        .filter(|string| !re.is_match(string))
        .collect::<Vec<_>>()
        .join(separator)
}

pub fn substitute(text: String, separator: &str, old_string: &str, new_string: &str, excludes: Vec<String>) -> String {
    let re = Regex::new(old_string).unwrap();
    text
        .split(separator)
        .map(|string| {
            let match_excludes = excludes.iter().any(|exclude| {
                let re_ex = Regex::new(exclude.as_str()).unwrap();
                re_ex.is_match(string)
            });
            if match_excludes {
                string.to_string()
            } else {
                re.replace_all(string, new_string).to_string()
            }
        })
        .collect::<Vec<_>>()
        .join(separator)
}
