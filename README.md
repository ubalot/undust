# Undust
A configurable cli tool for text manipulation

## Development
Build
```bash
cargo build
```

Run
```bash
cargo run -- -c config.yaml -i input.txt
```

### Run clippy
Simply run clippy
```bash
cargo clippy
```

Auto-correct rust code
```bash
cargo clippy --fix
```

### Run tests
```bash
cargo test
```

## Production
Compile with optimisations
```bash
cargo build --release
```

### Generate deb package
```bash
cargo deb
```
deb file will be in `target/debian/undust_X.Y.Z_amd64.deb`
