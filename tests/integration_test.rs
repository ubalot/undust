use undust;
use undust::apply_rules;
use undust::yaml::{ Config, HashMap };

#[test]
fn wrong_rules_enumeration() {
    let text = String::from(r#"test
fight for test
run for test
best test
"#);

    let yaml = String::from(r###"
rule10:
    operation: substitute
    args:
        old_string: best
        new_string: fast
        excludes:  # optional
            - test
"###);

    let expected_text = text.clone();

    let mut map = HashMap::from(yaml);
    let config = Config::from(&mut map);
    let modified_text = apply_rules(text, config.separator.as_str(), &map, false);
    assert_eq!(modified_text, expected_text);
}

#[test]
fn substitute_text() {
    let text = String::from(r#"test
fight for test
run for test
best test
"#);

    let yaml = String::from(r###"
rule1:
    operation: substitute
    args:
        old_string: best
        new_string: fast
        excludes:  # optional
            - test
"###);

    let expected_text = r#"test
fight for test
run for test
best test
"#;

    let mut map = HashMap::from(yaml);
    let config = Config::from(&mut map);
    let modified_text = apply_rules(text, config.separator.as_str(), &map, false);
    assert_eq!(modified_text, expected_text);
}

#[test]
fn delete_text() {
    let text = String::from(r#"test
fight for test
run for test
best test
"#);

    let yaml = String::from(r###"
rule1:
    operation: delete
    args:
        string: fight
"###);

    let expected_text = r#"test
run for test
best test
"#;

    let mut map = HashMap::from(yaml);
    let config = Config::from(&mut map);
    let modified_text = apply_rules(text, config.separator.as_str(), &map, false);
    assert_eq!(modified_text, expected_text);
}


#[test]
fn consecutive_substitutions() {
    let text = String::from(r#"test
fight for test
run for test
best test
"#);

    let yaml = String::from(r###"
rule1:
    operation: substitute
    args:
        old_string: best
        new_string: fast

rule2:
    operation: substitute
    args:
        old_string: ^fast
        new_string: belfast
"###);

    let expected_text = r#"test
fight for test
run for test
belfast test
"#;

    let mut map = HashMap::from(yaml);
    let config = Config::from(&mut map);
    let modified_text = apply_rules(text, config.separator.as_str(), &map, false);
    assert_eq!(modified_text, expected_text);
}


#[test]
fn all_together_now() {
    let text = String::from(r#"test
fight for test
run for test
best test
"#);

    let yaml = String::from(r###"
config:  # optional (every key is optional too)
    separator: "\n" # end of line (computer thinks it is a 1 char length)

rule1:
    operation: substitute
    args:
        old_string: best
        new_string: fast
        excludes:  # optional
            - test

rule2:
    operation: delete
    args:
        string: fight

rule3:
    operation: substitute
    args:
        old_string: ^fast$
        new_string: belfast
"###);

    let expected_text = r#"test
run for test
best test
"#;

    let mut map = HashMap::from(yaml);
    let config = Config::from(&mut map);
    let modified_text = apply_rules(text, config.separator.as_str(), &map, false);
    assert_eq!(modified_text, expected_text);
}

#[test]
fn simple_c_code() {
    let text = String::from(r###"
#include <stdio.h>

int main(int argc, char* argv[])
{
    printf("hello world\n");
    return 0;
}
"###);

    let yaml = String::from(r###"
config:  # optional (every key is optional too)
    separator: ";"

rule1:
    operation: substitute
    args:
        old_string: world
        new_string: earth
"###);

    let expected_text = r###"
#include <stdio.h>

int main(int argc, char* argv[])
{
    printf("hello earth\n");
    return 0;
}
"###;

    let mut map = HashMap::from(yaml);
    let config = Config::from(&mut map);
    let modified_text = apply_rules(text, config.separator.as_str(), &map, false);
    assert_eq!(modified_text, expected_text);
}
